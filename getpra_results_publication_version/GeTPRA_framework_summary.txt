Number of principal transcripts :  1748
Number of non-principal transcripts :  1707
Initial distribution
Single SL & single EC :	553 	 1041
Single SL & multiple ECs :	21 	 66
Multiple SLs & single EC :	471 	 2088
Multiple SLs & multiple ECs :	61 	 260
Exist in Recon 2M.1?
Single SL & single EC :	587 	 1121
Single SL & multiple ECs :	15 	 47
Multiple SLs & single EC :	414 	 1821
Multiple SLs & multiple ECs :	60 	 248
Blocked reaction?
Single SL & single EC :	100 	 215
Single SL & multiple ECs :	0 	 0
Multiple SLs & single EC :	11 	 34
Multiple SLs & multiple ECs :	0 	 0
Experimental evidence available?
Single SL & single EC :	23 	 50
Single SL & multiple ECs :	0 	 0
Multiple SLs & single EC :	0 	 0
Multiple SLs & multiple ECs :	0 	 0
BRENDA & UniProt : 	5441
HPA : 	924
BRENDA & UniProt & HPA : 	1125
No evidence : 	4178
