import argparse
import os
from cobra.io import read_sbml_model
from Bio import SeqIO

def get_peptide_sequence(output_dir, model_file, ensembl_mapping_file):
    cobra_model = read_sbml_model(model_file)
    genes = [gene.id for gene in cobra_model.genes]
    selected_ids = []
    with open(ensembl_mapping_file, 'r') as fp:
        fp.readline()
        for line in fp:            
            sptlist = line.strip().split('\t')
            if len(sptlist) > 2:
                if 'ENST' in sptlist[2].strip():
                    Entrez_gene_id = sptlist[0].strip()
                    transcript_id = sptlist[2].strip()
                    if Entrez_gene_id in genes:
                        selected_ids.append(transcript_id)
    
    human_gene_fa_file = './input_data/getpra_inputs/Homo_sapiens.GRCh38_peptide_sequences.fa'
    input_handle = open(human_gene_fa_file, "r")
    fp = open(output_dir+'/Ensembl_peptide_seq_metabolic_genes.fa', 'w')
    for seq_record in SeqIO.parse(input_handle, "fasta"):   
        header = seq_record.id
        ensembl_protein_id = header.split('|')[0].strip()
        ensembl_transcript_id = header.split('|')[1].strip()
        if ensembl_transcript_id in selected_ids:
            print >>fp, '>%s'%(seq_record.id)
            print >>fp, seq_record.seq      
    fp.close()
    
        
def main():    
    parser = argparse.ArgumentParser()
    parser.add_argument('-output_dir', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-model', '--cobra_model_file', required=True, help="Cobra model file")    
    parser.add_argument('-ensembl', '--Ensembl_mapping_file', required=True, help="Ensembl mapping file")

    options = parser.parse_args()   

    output_dir = options.output_dir
    model_file = options.cobra_model_file
    ensembl_mapping_file = options.Ensembl_mapping_file	
    
    get_peptide_sequence(output_dir, model_file, ensembl_mapping_file)
    
if __name__ == '__main__':
    main()