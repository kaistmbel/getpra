import argparse
import os
from cobra.io import read_sbml_model
from Bio import SeqIO

def get_EFICAz_WolfPSort_results(output_dir, model_file, ensembl_mapping_file, EFICAz25_output_file, WoLFPSort_output_file):
    precalculated_eficaz_file = EFICAz25_output_file
    precalculated_wolfpsort_file = WoLFPSort_output_file
        
    cobra_model = read_sbml_model(model_file)
    genes = [gene.id for gene in cobra_model.genes]
    selected_ids = []
    with open(ensembl_mapping_file, 'r') as fp:
        fp.readline()
        for line in fp:            
            sptlist = line.strip().split('\t')
            if len(sptlist) > 2:
                if 'ENST' in sptlist[2].strip():
                    Entrez_gene_id = sptlist[0].strip()
                    transcript_id = sptlist[2].strip()
                    if Entrez_gene_id in genes:
                        selected_ids.append(transcript_id)
        
    out_fp = open(output_dir+'Trimmed_EFICAz_result.txt', 'w')    
    with open(precalculated_eficaz_file, 'r') as fp:
        for line in fp:
            gene_transcript_info = line.split(',')[0].strip()
            ensembl_protein_id = gene_transcript_info.split('|')[0].strip()
            ensembl_transcript_id = gene_transcript_info.split('|')[1].strip()
            if ensembl_transcript_id in selected_ids:
                print >>out_fp, line.strip()
    out_fp.close()
    
    out_fp = open(output_dir+'Trimmed_WoLFPSort_result.txt', 'w')        
    with open(precalculated_wolfpsort_file, 'r') as fp:
        line = fp.readline()
        print >>out_fp, line.strip()
        for line in fp:
            gene_transcript_info = line.split(' ')[0].strip()
            ensembl_protein_id = gene_transcript_info.split('|')[0].strip()
            ensembl_transcript_id = gene_transcript_info.split('|')[1].strip()
            if ensembl_transcript_id in selected_ids:
                print >>out_fp, line.strip()
    out_fp.close()       
    
    
def main():    
    parser = argparse.ArgumentParser()
    parser.add_argument('-output_dir', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-model', '--cobra_model_file', required=True, help="Cobra model file")    
    parser.add_argument('-ensembl', '--Ensembl_mapping_file', required=True, help="Ensembl mapping file")
    parser.add_argument('-ec', '--EFICAz25_output_file', required=True, help="EFICAz output file")
    parser.add_argument('-sl', '--WoLFPSort_output_file', required=True, help="WoLFPSort output file")

    options = parser.parse_args()   
    WoLFPSort_output_file = options.WoLFPSort_output_file
    EFICAz25_output_file = options.EFICAz25_output_file
    output_dir = options.output_dir
    model_file = options.cobra_model_file
    ensembl_mapping_file = options.Ensembl_mapping_file	
    
    get_EFICAz_WolfPSort_results(output_dir, model_file, ensembl_mapping_file, EFICAz25_output_file, WoLFPSort_output_file)
    
if __name__ == '__main__':
    main()