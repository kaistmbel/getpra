import gzip


def read_gene_id_mapping_file(Ensembl_mapping_file, NCBI_mapping_file):
    entrez_gene_id_information = {}
    with gzip.open(NCBI_mapping_file, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split('\t')
            species_id = sptlist[0].strip()
            if species_id == '9606':
                entrez_gene_id = sptlist[1].strip()
                ensembl_gene_id = sptlist[2].strip()
                entrez_gene_id_information[entrez_gene_id] = ensembl_gene_id

    ensembl_id_mapping_information = {}
    with open(Ensembl_mapping_file, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split('\t')
            if 'ENSG' not in sptlist[0].strip():
                entrez_gene_id = sptlist[0].strip()
                ensembl_gene_id = sptlist[1].strip()
                ensembl_transcript_id = sptlist[2].strip()
                refseq_id = 'N/A'
                ucsc_id = 'N/A'
                for each_id in sptlist[3:]:
                    if 'NM_' in each_id:
                        refseq_id = each_id
                    if 'uc' in each_id:
                        ucsc_id = each_id.split('.')[0].strip()
                if entrez_gene_id in entrez_gene_id_information:
                    ensembl_id_from_ncbi = entrez_gene_id_information[entrez_gene_id]
                    if ensembl_id_from_ncbi == ensembl_gene_id:
                        if ensembl_transcript_id not in ensembl_id_mapping_information:
                            ensembl_id_mapping_information[ensembl_transcript_id] = {}
                            ensembl_id_mapping_information[ensembl_transcript_id]['entrez_gene_id'] = [entrez_gene_id]
                            ensembl_id_mapping_information[ensembl_transcript_id]['ensembl_gene_id'] = ensembl_gene_id
                            ensembl_id_mapping_information[ensembl_transcript_id][
                                'ensembl_transcript_id'] = ensembl_transcript_id
                            ensembl_id_mapping_information[ensembl_transcript_id]['refseq_id'] = refseq_id
                            ensembl_id_mapping_information[ensembl_transcript_id]['ucsc_id'] = ucsc_id
                        else:
                            ensembl_id_mapping_information[ensembl_transcript_id]['entrez_gene_id'].append(
                                entrez_gene_id)

    return ensembl_id_mapping_information


def read_APPRIS_file(APPRIS_mapping_file):
    principal_transcript_information = {}
    with open(APPRIS_mapping_file, 'r') as fp:
        for line in fp:
            if 'ENST' in line:
                if 'PRINCIPAL:' in line or 'ALTERNATIVE:' in line:
                    sptlist = line.strip().split('\t')
                    ensembl_transcript_id = 'N/A'
                    evidence = sptlist[-1].strip()
                    for each_id in sptlist:
                        if 'ENST' in each_id:
                            ensembl_transcript_id = each_id
                    principal_transcript_information[ensembl_transcript_id] = evidence

    return principal_transcript_information


def read_EC_prediction_result(EC_prediction_file):
    ec_prediction_info = {}
    with open(EC_prediction_file, 'r') as fp:
        for line in fp:
            sptlist = line.strip().split('\t')
            ensembl_transcript_id = sptlist[1].strip()
            predicted_ec_number = sptlist[2].strip()

            if ensembl_transcript_id not in ec_prediction_info:
                ec_prediction_info[ensembl_transcript_id] = [predicted_ec_number]
            else:
                ec_prediction_info[ensembl_transcript_id].append(predicted_ec_number)

    return ec_prediction_info


def read_SL_prediction_result(SL_prediction_file):
    sl_prediction_info = {}
    with open(SL_prediction_file, 'r') as fp:
        for line in fp:
            sptlist = line.strip().split('\t')
            ensembl_transcript_id = sptlist[1].strip()
            predicted_sl_list = sptlist[2].strip().split(',')
            sl_prediction_info[ensembl_transcript_id] = predicted_sl_list
    return sl_prediction_info


def read_SL_evidence_result(SL_evidence_file):
    sl_evidence_info = {}
    with open(SL_evidence_file, 'r') as fp:
        for line in fp:
            sptlist = line.strip().split('\t')
            ensembl_gene_id = sptlist[0].strip()
            sl_information = sptlist[1].strip()
            sl_evidence_info[ensembl_gene_id] = sl_information.split(';')
    return sl_evidence_info


def read_EC_evidence_result(BRENDA_EC_evidence_file, UniProt_evidence_file):
    uniprot_id_info = {}
    fp = open(UniProt_evidence_file, 'r')
    fp.readline()
    for line in fp:
        sptlist = line.strip().split('\t')
        if len(sptlist) == 2:
            uniprot_id = sptlist[0].strip()
            ensembl_transcipt_id = sptlist[1].strip()
            if uniprot_id not in uniprot_id_info:
                uniprot_id_info[uniprot_id] = [ensembl_transcipt_id]
            else:
                uniprot_id_info[uniprot_id].append(ensembl_transcipt_id)

    fp.close()
    
    ec_evidence_info = {}
    transcript_uniprot_info = {}
    fp = open(BRENDA_EC_evidence_file, 'r')
    for line in fp:
        sptlist = line.strip().split('\t')
        if len(sptlist) == 2:
            ec_number = sptlist[0].strip()
            uniprot_id_list = sptlist[1].strip().split(';')
            transcript_id_list = []
            for each_uniprot_id in uniprot_id_list:
                if each_uniprot_id in uniprot_id_info:
                    ensembl_transcript_id_list = uniprot_id_info[each_uniprot_id]
                    for each_transcript in ensembl_transcript_id_list:
                        transcript_id_list.append(each_transcript)

                        if each_transcript not in transcript_uniprot_info:
                            transcript_uniprot_info[each_transcript] = [each_uniprot_id]
                        else:
                            transcript_uniprot_info[each_transcript].append(each_uniprot_id)

            ec_evidence_info[ec_number] = transcript_id_list
    fp.close()

    return ec_evidence_info, transcript_uniprot_info


def read_reaction_information_file(reaction_file):
    reaction_information = {}
    with open(reaction_file, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split('\t')
            ec_number = sptlist[0].strip()
            reaction_id = sptlist[1].strip()
            reaction_name = sptlist[2].strip()
            pathway = sptlist[3].strip()
            kegg_equation = sptlist[4].strip()
            mnxref_equation = sptlist[5].strip()
            definition = sptlist[6].strip()

            record = {}
            record['ec_number'] = ec_number
            record['reaction_id'] = reaction_id
            record['reaction_name'] = reaction_name
            record['pathway'] = pathway
            record['kegg_equation'] = kegg_equation
            record['mnxref_equation'] = mnxref_equation
            record['definition'] = definition

            if ec_number not in reaction_information:
                reaction_information[ec_number] = [record]
            else:
                reaction_information[ec_number].append(record)

    return reaction_information
