import re
import urllib2
import sys
import time
import logging

def check_transferred_ec(ecnumber):
    url = "http://rest.kegg.jp/get/enzyme:%s" % (ecnumber)
    ecinfo_text = urllib2.urlopen(url).read()
    for line in ecinfo_text.split('\n'):
        if 'NAME        Transferred to ' in line:
            new_EC = line.split('NAME        Transferred to ')[1].strip()
            return new_EC
    return False


def get_reactions_with_pathway_from_EC_number(ecnumber):
    new_ec = check_transferred_ec(ecnumber)
    if new_ec != False:
        ecnumber = new_ec

    url = "http://rest.kegg.jp/get/enzyme:%s" % (ecnumber)
    ecinfo_text = urllib2.urlopen(url).read()
    reaction_set = re.findall('\[RN:[\w\s]+\]', ecinfo_text)
    reaction_list = []
    for each_set in reaction_set:
        reaction_candidates = re.findall('R[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+', each_set)
        reaction_list += reaction_candidates
    reaction_list = list(set(reaction_list))

    pathway_list = []
    for line in ecinfo_text.split('\n'):
        temp_pathway_list = re.findall('ec[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+', line)
        if temp_pathway_list != []:
            pathway = line.split(temp_pathway_list[0].strip())[1].strip()
            pathway_list.append(pathway)

    pathway_list = list(set(pathway_list))
    return reaction_list, pathway_list


def get_reaction_info(reaction):
    reaction_info_dict = {}
    url = "http://rest.kegg.jp/get/rn:%s" % (reaction)
    reaction_info_text = urllib2.urlopen(url).read()
    splited_text = reaction_info_text.strip().split('\n')
    NAME = ''
    EQUATION = ''
    DEFINITION = ''
    for line in splited_text:
        sptlist = line.split()
        if sptlist[0].strip() == 'NAME':
            NAME = ' '.join(sptlist[1:])
        if sptlist[0].strip() == 'DEFINITION':
            DEFINITION = ' '.join(sptlist[1:])
        if sptlist[0].strip() == 'EQUATION':
            EQUATION = ' '.join(sptlist[1:])
    return {'name': NAME, 'definition': DEFINITION, 'equation': EQUATION}


def convert_KEGG_metabolite_id_to_MNXref(equation, mnxref_information):
    compound_list = [each_component for each_component in equation.split(' ') if each_component[0].isalpha()]
    for each_compound_id in compound_list:
        if each_compound_id in mnxref_information:
            mnx_id = mnxref_information[each_compound_id]
            equation = equation.replace(each_compound_id, mnx_id)
        else:
            return False

    if len(compound_list) != equation.count('MNXM'):
        return False
    else:
        return equation


def read_MNXref_information(metanetx_compound_xref_file, metabolic_db='kegg'):
    mnxref_information = {}
    with open(metanetx_compound_xref_file, 'r') as fp:
        for line in fp:
            if line[0] != '#':
                sptlist = line.strip().split('\t')
                metabolite_id = sptlist[0].strip()
                db_name = metabolite_id.split(':')[0]
                db_metabolite_id = metabolite_id.split(':')[1]
                if db_name == metabolic_db:
                    mnx_id = sptlist[1].strip()
                    mnxref_information[db_metabolite_id] = mnx_id
    return mnxref_information


def download_KEGG_data(ec_number_file, mnxref_com_xref_file, kegg_reaction_output_file):   
    logging.info("Downloading KEGG data")
    start = time.time()
    
    mnxref_information = read_MNXref_information(mnxref_com_xref_file, 'kegg')

    ec_numbers = []
    with open(ec_number_file, 'r') as fp:
        for line in fp:
            ec_numbers.append(line.strip())
            
    logging.info("%s EC numbers found"%(len(ec_numbers)))
                 
    result_data_list = []
    cnt = 0
    for each_ecnumber in ec_numbers:        
        try:
            cnt += 1       
            if cnt % (len(ec_numbers)/10) == 0:            
                logging.info("Downloading KEGG data %.2f %% (%s/%s) completed"%((cnt/float(len(ec_numbers)))*100, cnt, len(ec_numbers)))
            reactions, pathway_list = get_reactions_with_pathway_from_EC_number(each_ecnumber)
            pathway_str = ';'.join(pathway_list)
            for each_reaction in reactions:
                reaction_info = get_reaction_info(each_reaction)
                if 'equation' in reaction_info:
                    if reaction_info['equation'] != '':
                        kegg_equation = reaction_info['equation']
                        mnx_id_equation = convert_KEGG_metabolite_id_to_MNXref(kegg_equation, mnxref_information)
                        record = {}
                        record['ec'] = each_ecnumber
                        record['reaction'] = each_reaction
                        record['pathway'] = pathway_str
                        record['name'] = reaction_info['name']
                        record['kegg_equation'] = reaction_info['equation']
                        record['mnxref_equation'] = mnx_id_equation
                        record['definition'] = reaction_info['definition']
                        result_data_list.append(record)
        except:
            pass

    with open(kegg_reaction_output_file, 'w') as fp:
        print >> fp, 'EC\tReaction\tName\tPathway\tKEGG equation\tMNXref equation\tDefinition'
        for each_record in result_data_list:
            print >> fp, '%s\t%s\t%s\t%s\t%s\t%s\t%s' % (
                each_record['ec'], each_record['reaction'], each_record['name'], each_record['pathway'],
                each_record['kegg_equation'], each_record['mnxref_equation'], each_record['definition'])
    
    logging.info("Downloading KEGG data completed, "+time.strftime("elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
                 
                 
                 