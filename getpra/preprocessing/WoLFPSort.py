def parse_WoLFPSort_result(WoLFPSort_result_file, gene_annotation_file, output_file):
    convert_info = {}
    convert_info['cyto'] = 'c'
    convert_info['mito'] = 'm'
    convert_info['E.R.'] = 'r'
    convert_info['nucl'] = 'n'
    convert_info['pero'] = 'x'
    convert_info['lyso'] = 'l'
    convert_info['golg'] = 'g'

    protein_coding_transcript_info = {}
    fp = open(gene_annotation_file, 'r')
    fp.readline()
    for line in fp:
        sptlist = line.strip().split('\t')
        for each_id in sptlist:
            if 'ENST' in each_id:
                transcript_id = each_id
        protein_coding_transcript_info[transcript_id] = transcript_id
    fp.close()

    fp = open(WoLFPSort_result_file, 'r')
    fp2 = open(output_file, 'w')
    fp.readline()
    for line in fp:
        peptide_transcript_id = line.strip().split(' ')[0].strip()
        localization_information_list = line.strip().split(' ')
        localization_information_string = ' '.join(localization_information_list[1:])

        peptide_id = peptide_transcript_id.split('|')[0]
        transcript_id = peptide_transcript_id.split('|')[1]

        localization_list = [each_data.strip().split(' ')[0].strip() for each_data in
                             localization_information_string.split(',')]
        new_localization_list = []

        if transcript_id in protein_coding_transcript_info:
            for each_localization in localization_list:
                if each_localization in convert_info:
                    new_compartment = convert_info[each_localization]
                    new_localization_list.append(new_compartment)

            if len(new_localization_list) > 0:
                print >> fp2, '%s\t%s\t%s' % (peptide_id, transcript_id, ','.join(new_localization_list))
    fp.close()
    fp2.close()
