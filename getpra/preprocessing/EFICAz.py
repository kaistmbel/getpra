def parse_EFICAz_result(EFICAz_result_file, output_file, ec_number_output_file):
    ec_number_list = []
    out_fp = open(output_file, 'w')
    with open(EFICAz_result_file, 'r') as fp:
        for line in fp:
            sptlist = line.strip().split(',')
            if 'No EFICAz EC assignment' not in line:
                if '4EC:' in line:
                    peptide_transcript_id = sptlist[0].strip()
                    peptide_id = peptide_transcript_id.split('|')[0]
                    transcript_id = peptide_transcript_id.split('|')[1]
                    ec = sptlist[1].strip()
                    ec = ec.replace('4EC: ', '').strip()
                    print >> out_fp, '%s\t%s\t%s' % (peptide_id, transcript_id, ec)
                    ec_number_list.append(ec.strip())
    out_fp.close()
    ec_number_list = list(set(ec_number_list))
    fp = open(ec_number_output_file, 'w')
    for each_ec_number in ec_number_list:
        print >> fp, each_ec_number
    fp.close()
