import urllib
import urllib2
import time
import logging

def download_UniProt_data(output_file):
    logging.info("Downloading UniProt data")
    start = time.time()
    uniprot_result_information = {}
    accession_code_list = []
    url = "http://www.uniprot.org/uniprot/?query=reviewed:yes+AND+organism:9606&format=tab&columns=id,ec"
    reviewed_uniprot_text = urllib2.urlopen(url).read()

    for each_line in reviewed_uniprot_text.split('\n'):
        sptlist = each_line.split('\t')
        accession_code = sptlist[0].strip()
        accession_code_list.append(accession_code)

    accession_code_list = list(set(accession_code_list))
    url = 'http://www.uniprot.org/uploadlists/'
    params = {
        'from': 'ACC',
        'to': 'ENSEMBL_TRS_ID',
        'format': 'tab',
        'query': ' '.join(accession_code_list)
    }

    data = urllib.urlencode(params)
    request = urllib2.Request(url, data)
    contact = ""
    request.add_header('User-Agent', 'Python %s' % contact)
    response = urllib2.urlopen(request)
    page = response.read()

    with open(output_file, 'w') as fp:
        for each_line in page.split('\n'):
            print >> fp, each_line
            
    logging.info("Downloading UniProt data completed, " + time.strftime("elapsed time %H:%M:%S", time.gmtime(time.time() - start)))