import os
import time
import logging
import pandas as pd


def parse_HPA_file(HPA_file, HPA_output_file):
    logging.info("Preprocessing HPA data")
    start = time.time()
    compartment_Info = {}
    compartment_Info['Cytoplasm'] = 'c'
    compartment_Info['Cytosol'] = 'c'
    compartment_Info['Nucleus but not nucleoli'] = 'n'
    compartment_Info['Mitochondria'] = 'm'
    compartment_Info['Nucleus'] = 'n'
    compartment_Info['Lysosomes'] = 'l'
    compartment_Info['Peroxisomes'] = 'x'
    compartment_Info['Endoplasmic reticulum'] = 'r'
    compartment_Info['Golgi apparatus'] = 'g'

    HPA_sl_df = pd.read_csv(HPA_file)
    HPA_sl_df = HPA_sl_df[HPA_sl_df['Reliability'] != 'Uncertain']
    HPA_sl_df = HPA_sl_df[['Validated', 'Supported', 'Approved', 'Gene']]

    HPA_evidence_information = {}
    for each_row, each_df in HPA_sl_df.iterrows():
        Ensembl_gene_id = each_df['Gene']
        compartment_list = []
        for each_confidence_level in ['Validated', 'Supported', 'Approved']:
            primary_compartment_string = str(each_df[each_confidence_level])

            for each_compartment in primary_compartment_string.split(';'):
                if each_compartment in compartment_Info:
                    compartment_id = compartment_Info[each_compartment]
                    compartment_list.append(compartment_id)

        compartment_list = list(set(compartment_list))
        if Ensembl_gene_id not in HPA_evidence_information:
            HPA_evidence_information[Ensembl_gene_id] = compartment_list
        else:
            HPA_evidence_information[Ensembl_gene_id] += compartment_list
            HPA_evidence_information[Ensembl_gene_id] = list(set(HPA_evidence_information[Ensembl_gene_id]))

    with open(HPA_output_file, 'w') as fp:
        for each_gene in HPA_evidence_information:
            if len(HPA_evidence_information[each_gene]) > 0:
                print >> fp, '%s\t%s' % (each_gene, ';'.join(HPA_evidence_information[each_gene]))
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    return HPA_evidence_information



