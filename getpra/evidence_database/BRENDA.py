import hashlib
import logging
import time
import logging
from SOAPpy import SOAPProxy

def check_BRENDA_connection(email_address, user_password):
    logging.info("Checking BRENDA connection")    
    endpointURL = "http://www.brenda-enzymes.org/soap/brenda_server.php"
    client = SOAPProxy(endpointURL)
    password = hashlib.sha256(user_password).hexdigest()
            
    parameters = "%s," % (email_address) + password + "," + "organism*Homo sapiens#"
    resultString = client.getSequence(parameters)
    
    if 'Unknown user. Please register. www.brenda-enzymes.org/register.php' in resultString:
        logging.info("BRENDA connection failed ("+resultString+")")    
        return False
    else:
        logging.info("BRENDA server connected")    
        return True

def download_BRENDA_data(ec_number_file, output_file, email_address, user_password):    
    logging.info("Downloading BRENDA data")
    start = time.time()
    endpointURL = "http://www.brenda-enzymes.org/soap/brenda_server.php"
    client = SOAPProxy(endpointURL)
    password = hashlib.sha256(user_password).hexdigest()

    ec_numbers = []
    fp = open(ec_number_file, 'r')
    for line in fp:
        ec_numbers.append(line.strip())
    fp.close()

    ecnumber_uniprot_info = {}
    cnt = 0
    for each_ecnumber in ec_numbers:
        cnt += 1
        if cnt % (len(ec_numbers)/10) == 0:            
            logging.info("Downloading BRENDA data %.2f %% (%s/%s) completed"%((cnt/float(len(ec_numbers)))*100, cnt, len(ec_numbers)))
            
        parameters = "%s," % (email_address) + password + "," + "ecNumber*%s#organism*Homo sapiens#" % each_ecnumber
        
        resultString = client.getSequence(parameters)
        
        accession_codes = []
        for each_data in resultString.split('#'):
            if 'firstAccessionCode*' in each_data:
                accession_code = each_data.split('firstAccessionCode*')[1].strip()
                accession_codes.append(accession_code)
        
        accession_codes = list(set(accession_codes))
        ecnumber_uniprot_info[each_ecnumber] = accession_codes

    fp = open(output_file, 'w')
    for each_ecnumber in ecnumber_uniprot_info:
        print >> fp, '%s\t%s' % (each_ecnumber, ';'.join(ecnumber_uniprot_info[each_ecnumber]))
    fp.close()

    logging.info("Downloading BRENDA data completed, "+time.strftime("elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    
def read_UniProt_accession(filename):
    uniprot_accession_info = {}
    fp = open(filename, 'r')
    for line in fp:
        if 'Ensembl_TRS' in line:
            sptlist = line.strip().split('\t')
            transcript_id = sptlist[2].strip()
            accession_code = sptlist[0].strip()
            uniprot_accession_info[accession_code] = transcript_id
    fp.close()
    return uniprot_accession_info


def parse_BRENDA_file(uniprot_review_information_file, uniprot_accession_info, brenda_output_file, output_file):
    reviewed_accession_codes = []
    fp = open(uniprot_review_information_file, 'r')
    fp.readline()
    for line in fp:
        sptlist = line.strip().split('\t')
        acession_code = sptlist[0].strip()
        reviewed_accession_codes.append(acession_code)
    fp.close()
    reviewed_accession_codes = list(set(reviewed_accession_codes))

    fp = open(brenda_output_file, 'r')
    fp2 = open(output_file, 'w')
    fp.readline()
    for line in fp:
        sptlist = line.strip().split('\t')
        if len(sptlist) == 2:
            ec_number = sptlist[0].strip()
            acession_list = sptlist[1].strip().split(';')
            acession_list = list(set(acession_list) & set(reviewed_accession_codes))
            for each_accession in acession_list:
                if each_accession in uniprot_accession_info:
                    transcript_id = uniprot_accession_info[each_accession]
                    print >> fp2, '%s\t%s\t%s' % (ec_number, each_accession, transcript_id)
    fp.close()
    fp2.close()
