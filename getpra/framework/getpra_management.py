import pandas as pd


def read_redundant_reaction(getpra_cobra_model, redundant_reaction_file):
    redundant_reaction_info = {}
    getpra_reaction_name_info = {}
    for each_reaction in getpra_cobra_model.reactions:
        getpra_reaction_name_info[each_reaction.id] = each_reaction.name
        redundant_reaction_info[each_reaction.name] = []

    with open(redundant_reaction_file, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split('\t')
            model_reaction = sptlist[0].strip()
            getpra_model_reaction = sptlist[1].strip()
            getpra_model_reaction_name = getpra_reaction_name_info[getpra_model_reaction]
            redundant_reaction_info[getpra_model_reaction_name].append(model_reaction)

    return redundant_reaction_info


def read_reaction_functionality(getpra_cobra_model, reaction_functionality_file):
    reaction_functionality_info = {}
    getpra_reaction_name_info = {}
    for each_reaction in getpra_cobra_model.reactions:
        getpra_reaction_name_info[each_reaction.id] = each_reaction.name
        reaction_functionality_info[each_reaction.name] = []

    with open(reaction_functionality_file, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split('\t')
            getpra_model_reaction = sptlist[0].strip()
            getpra_model_reaction_name = getpra_reaction_name_info[getpra_model_reaction]
            functionality = sptlist[1].strip()
            isblocked_reaction = 'N/A'
            if functionality == 'True':
                isblocked_reaction = 'No'
            else:
                isblocked_reaction = 'Yes'
            reaction_functionality_info[getpra_model_reaction_name] = isblocked_reaction

    return reaction_functionality_info


def update_functionality_information_to_GeTPRA(getpra_cobra_model, getpra_file, reaction_functionality_file, redundant_reaction_file,
                  updated_getpra_output_file):
    redundant_reaction_info = read_redundant_reaction(getpra_cobra_model, redundant_reaction_file)
    reaction_functionality_info = read_reaction_functionality(getpra_cobra_model, reaction_functionality_file)

    fp2 = open(updated_getpra_output_file, 'w')
    fp = open(getpra_file, 'r')
    line = fp.readline()
    print >> fp2, '%s\t%s\t%s' % (line.strip(), 'Exist in Recon 2M? (Overlapping reaction ID)', 'Blocked reaction?')
    for line in fp:
        sptlist = line.strip().split('\t')
        ensembl_transcript_id = sptlist[2].strip()
        ec_number = sptlist[6].strip()
        compartment = sptlist[8].strip()
        kegg_reaction_id = sptlist[10].strip()

        existence_information = 'N/A'
        is_blocked = 'N/A'

        model_id = '%s_%s_%s_%s' % (kegg_reaction_id, ec_number, ensembl_transcript_id, compartment)
        if model_id in redundant_reaction_info:
            redundant_reactions = redundant_reaction_info[model_id]
            if len(redundant_reactions) == 0:
                existence_information = 'N/A'
            else:
                existence_information = ';'.join(redundant_reactions)

        if model_id in reaction_functionality_info:
            is_blocked = reaction_functionality_info[model_id]

        print >> fp2, '%s\t%s\t%s' % (line.strip(), existence_information, is_blocked)

    fp.close()
    return


def summarize_GeTPRA_using_four_categories(df):
    data_info = {}
    for each_row, each_df in df.iterrows():
        gene_id = each_df['Ensembl gene ID']
        transcript_id = each_df['Ensembl transcript ID']
        if gene_id not in data_info:
            data_info[gene_id] = {}
        if transcript_id not in data_info[gene_id]:
            data_info[gene_id][transcript_id] = {}
            data_info[gene_id][transcript_id]['ec'] = []
            data_info[gene_id][transcript_id]['sl'] = []

        ec = each_df['Predicted EC number']
        sl = each_df['Predicted SL']

        data_info[gene_id][transcript_id]['ec'].append(ec)
        data_info[gene_id][transcript_id]['sl'].append(sl)

    single_SL_single_EC = 0
    single_SL_multiple_ECs = 0
    multiple_SLs_single_EC = 0
    multiple_SLs_multiple_ECs = 0

    single_SL_single_EC_transcripts = 0
    single_SL_multiple_ECs_transcripts = 0
    multiple_SLs_single_EC_transcripts = 0
    multiple_SLs_multiple_ECs_transcripts = 0

    for each_gene in data_info:

        ec_total_list = []
        sl_total_list = []
        temp_transcript_cnt = 0
        for each_transcript in data_info[each_gene]:
            ec_list = data_info[each_gene][each_transcript]['ec']
            sl_list = data_info[each_gene][each_transcript]['sl']
            ec_list = list(set(ec_list))
            sl_list = list(set(sl_list))
            ec_total_list.append(ec_list)
            sl_total_list.append(sl_list)

            if len(ec_list) > 0 or len(sl_list) > 0:
                temp_transcript_cnt += 1

        unique_ec_list = []
        unique_sl_list = []
        for each_ec in ec_total_list:
            if each_ec not in unique_ec_list:
                unique_ec_list.append(each_ec)

        for each_sl in sl_total_list:
            if each_sl not in unique_sl_list:
                unique_sl_list.append(each_sl)

        if len(unique_ec_list) == 1 and len(unique_sl_list) == 1:
            single_SL_single_EC += 1
            single_SL_single_EC_transcripts += temp_transcript_cnt
        if len(unique_ec_list) > 1 and len(unique_sl_list) == 1:
            single_SL_multiple_ECs += 1
            single_SL_multiple_ECs_transcripts += temp_transcript_cnt
        if len(unique_ec_list) == 1 and len(unique_sl_list) > 1:
            multiple_SLs_single_EC += 1
            multiple_SLs_single_EC_transcripts += temp_transcript_cnt
        if len(unique_ec_list) > 1 and len(unique_sl_list) > 1:
            multiple_SLs_multiple_ECs += 1
            multiple_SLs_multiple_ECs_transcripts += temp_transcript_cnt

    single_SL_single_EC_result = '%s \t %s' % (single_SL_single_EC, single_SL_single_EC_transcripts)
    single_SL_multiple_ECs_result = '%s \t %s' % (single_SL_multiple_ECs, single_SL_multiple_ECs_transcripts)
    multiple_SLs_single_EC_result = '%s \t %s' % (multiple_SLs_single_EC, multiple_SLs_single_EC_transcripts)
    multiple_SLs_multiple_ECs_result = '%s \t %s' % (multiple_SLs_multiple_ECs, multiple_SLs_multiple_ECs_transcripts)

    return single_SL_single_EC_result, single_SL_multiple_ECs_result, multiple_SLs_single_EC_result, multiple_SLs_multiple_ECs_result


def summarize_GeTPRA_evidence(df):
    all_index = list(df.index)

    ec_evidence_df = df[df['Experimental evidences on ECs (UnitProt ID)'].isnull() == False]
    sl_evidence_df = df[df['Experimental evidences on SLs'].isnull() == False]

    ec_without_evidence_df = df[df['Experimental evidences on ECs (UnitProt ID)'].isnull()]
    sl_without_evidence_df = df[df['Experimental evidences on SLs'].isnull()]

    ec_evidence_index = list(ec_evidence_df.index)
    sl_evidence_index = list(sl_evidence_df.index)

    ec_without_evidence_index = list(ec_without_evidence_df.index)
    sl_without_evidence_index = list(sl_without_evidence_df.index)

    both_ec_sl_evidence_index = list(set(ec_evidence_index) & set(sl_evidence_index))
    both_ec_sl_without_evidence_index = list(set(ec_without_evidence_index) & set(sl_without_evidence_index))

    single_evidence_index = set(all_index).difference(
        set(both_ec_sl_evidence_index + both_ec_sl_without_evidence_index))

    ec_evidence_index = list(set(ec_evidence_index) & set(single_evidence_index))
    sl_evidence_index = list(set(sl_evidence_index) & set(single_evidence_index))

    all_evidence_cnt = len(both_ec_sl_evidence_index)
    no_evidence_cnt = len(both_ec_sl_without_evidence_index)
    SL_evidence_cnt = len(sl_evidence_index)
    EC_evidence_cnt = len(ec_evidence_index)

    return all_evidence_cnt, no_evidence_cnt, EC_evidence_cnt, SL_evidence_cnt


def check_evidence_and_functionality(output_dir, getpra_file):
    with open(output_dir + 'GeTPRA_framework_summary.txt', 'w') as fp:
        df = pd.read_table(getpra_file)
        ec_evidence_df = df[df['Experimental evidences on ECs (UnitProt ID)'].isnull() == False]
        sl_evidence_df = df[df['Experimental evidences on SLs'].isnull() == False]

        ec_without_evidence_df = df[df['Experimental evidences on ECs (UnitProt ID)'].isnull()]
        sl_without_evidence_df = df[df['Experimental evidences on SLs'].isnull()]

        ec_evidence_index = list(ec_evidence_df.index)
        sl_evidence_index = list(sl_evidence_df.index)

        ec_without_evidence_index = list(ec_without_evidence_df.index)
        sl_without_evidence_index = list(sl_without_evidence_df.index)

        no_evidence_index = list(set(ec_without_evidence_index) & set(sl_without_evidence_index))
        EC_evidence_only_index = list(set(ec_evidence_index).difference(sl_evidence_index))
        SL_evidence_only_index = list(set(sl_evidence_index).difference(ec_evidence_index))
        Both_evidence_index = list(set(ec_evidence_index) & set(sl_evidence_index))

        EC_evidence_only_cnt = len(EC_evidence_only_index)
        SL_evidence_only_cnt = len(SL_evidence_only_index)
        Both_evidence_cnt = len(Both_evidence_index)
        no_evidence_cnt = len(no_evidence_index)

        PT = list(set(df[df['Transcript type'] != 'NPT']['Ensembl transcript ID']))
        NPT = list(set(df[df['Transcript type'] == 'NPT']['Ensembl transcript ID']))

        print >> fp, 'Number of principal transcripts : ', len(PT)
        print >> fp, 'Number of non-principal transcripts : ', len(NPT)

        single_SL_single_EC, single_SL_multiple_ECs, multiple_SLs_single_EC, multiple_SLs_multiple_ECs = summarize_GeTPRA_using_four_categories(
            df)
        print >> fp, 'Initial distribution'
        print >> fp, 'Single SL & single EC :\t', single_SL_single_EC
        print >> fp, 'Single SL & multiple ECs :\t', single_SL_multiple_ECs
        print >> fp, 'Multiple SLs & single EC :\t', multiple_SLs_single_EC
        print >> fp, 'Multiple SLs & multiple ECs :\t', multiple_SLs_multiple_ECs

        GeTPRA_step1_df = df[df['Exist in Recon 2M? (Overlapping reaction ID)'].isnull()]
        single_SL_single_EC, single_SL_multiple_ECs, multiple_SLs_single_EC, multiple_SLs_multiple_ECs = summarize_GeTPRA_using_four_categories(
            GeTPRA_step1_df)
        print >> fp, 'Exist in Recon 2M.1?'
        print >> fp, 'Single SL & single EC :\t', single_SL_single_EC
        print >> fp, 'Single SL & multiple ECs :\t', single_SL_multiple_ECs
        print >> fp, 'Multiple SLs & single EC :\t', multiple_SLs_single_EC
        print >> fp, 'Multiple SLs & multiple ECs :\t', multiple_SLs_multiple_ECs

        GeTPRA_step2_df = GeTPRA_step1_df[GeTPRA_step1_df['Blocked reaction?'] == 'No']
        single_SL_single_EC, single_SL_multiple_ECs, multiple_SLs_single_EC, multiple_SLs_multiple_ECs = summarize_GeTPRA_using_four_categories(
            GeTPRA_step2_df)
        print >> fp, 'Blocked reaction?'
        print >> fp, 'Single SL & single EC :\t', single_SL_single_EC
        print >> fp, 'Single SL & multiple ECs :\t', single_SL_multiple_ECs
        print >> fp, 'Multiple SLs & single EC :\t', multiple_SLs_single_EC
        print >> fp, 'Multiple SLs & multiple ECs :\t', multiple_SLs_multiple_ECs

        GeTPRA_step3_df = GeTPRA_step2_df[
            GeTPRA_step2_df['Experimental evidences on ECs (UnitProt ID)'].isnull() == False]
        GeTPRA_step3_df = GeTPRA_step3_df[GeTPRA_step3_df['Experimental evidences on SLs'].isnull() == False]

        single_SL_single_EC, single_SL_multiple_ECs, multiple_SLs_single_EC, multiple_SLs_multiple_ECs = summarize_GeTPRA_using_four_categories(
            GeTPRA_step3_df)
        print >> fp, 'Experimental evidence available?'
        print >> fp, 'Single SL & single EC :\t', single_SL_single_EC
        print >> fp, 'Single SL & multiple ECs :\t', single_SL_multiple_ECs
        print >> fp, 'Multiple SLs & single EC :\t', multiple_SLs_single_EC
        print >> fp, 'Multiple SLs & multiple ECs :\t', multiple_SLs_multiple_ECs

        all_evidence_cnt, no_evidence_cnt, EC_evidence_cnt, SL_evidence_cnt = summarize_GeTPRA_evidence(df)

        print >> fp, 'BRENDA & UniProt : \t', EC_evidence_cnt
        print >> fp, 'HPA : \t', SL_evidence_cnt
        print >> fp, 'BRENDA & UniProt & HPA : \t', all_evidence_cnt
        print >> fp, 'No evidence : \t', no_evidence_cnt
    
    GeTPRA_step3_df.to_csv(output_dir + 'GeTPRA_with_evidence.csv', index=False)
    

def check_reaction_evidence(evidence_redundant_getpra_df, cobra_reaction):
    ec_evidence_df = evidence_redundant_getpra_df[
        evidence_redundant_getpra_df['Experimental evidences on ECs (UnitProt ID)'].isnull() == False]
    high_evidence_redundant_getpra_df = ec_evidence_df[
        ec_evidence_df['Experimental evidences on SLs'].isnull() == False]

    cobra_reaction_genes = [gene.id for gene in cobra_reaction.genes]
    cobra_reaction_genes = list(set(cobra_reaction_genes))

    getpra_genes = list(high_evidence_redundant_getpra_df['Entrez gene ID'])
    getpra_genes = [str(int(gene)) for gene in getpra_genes]
    getpra_genes = list(set(getpra_genes))

    evidence_genes = list(set(getpra_genes) & set(cobra_reaction_genes))
    low_evidence_genes = list(set(cobra_reaction_genes).difference(set(getpra_genes)))

    if set(getpra_genes) != set(cobra_reaction_genes):
        return getpra_genes, cobra_reaction_genes, evidence_genes, low_evidence_genes
    else:
        return getpra_genes, cobra_reaction_genes, False, False

def check_GPR_evidence(output_dir, cobra_model, getpra_file):
    output_file = output_dir + 'GPR_comparison_result.txt'
    with open(output_file, 'w') as fp:
        print >> fp, 'Evidence type\tReaction ID\tGeTPRA genes\tModel genes\tModel genes with GeTPRA evidences\tModel genes without GeTPRA evidences'
        df = pd.read_table(getpra_file)
        redundant_getpra_df = df[df['Exist in Recon 2M? (Overlapping reaction ID)'].isnull() == False]
        redundant_reaction_index_info = {}
        for each_index, each_df in redundant_getpra_df.iterrows():
            redundant_reaction_string = each_df['Exist in Recon 2M? (Overlapping reaction ID)']
            redundant_reactions = redundant_reaction_string.split(';')
            for each_reaction in redundant_reactions:
                if each_reaction not in redundant_reaction_index_info:
                    redundant_reaction_index_info[each_reaction] = [each_index]
                else:
                    redundant_reaction_index_info[each_reaction].append(each_index)

        for each_reaction in cobra_model.reactions:
            if each_reaction.id[0:3] != 'DM_' and each_reaction.id[0:3] != 'EX_':
                if each_reaction.id in redundant_reaction_index_info:
                    target_index_list = redundant_reaction_index_info[each_reaction.id]
                    getpra_genes, cobra_reaction_genes, evidence_genes, low_evidence_genes = check_reaction_evidence(
                        redundant_getpra_df.ix[target_index_list], each_reaction)
                    if evidence_genes != False:
                        print >> fp, 'Inconsistent GPR\t%s\t%s\t%s\t%s\t%s' % (
                            each_reaction.id, ';'.join(getpra_genes), ';'.join(cobra_reaction_genes),
                            ';'.join(evidence_genes), ';'.join(low_evidence_genes))
                    else:
                        print >> fp, 'Consistent GPR\t%s\t%s\t%s\tN/A\tN/A' % (
                            each_reaction.id, ';'.join(getpra_genes), ';'.join(cobra_reaction_genes))
