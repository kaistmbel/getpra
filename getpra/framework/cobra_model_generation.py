from cobra import Model, Reaction, Metabolite
import logging
import time

def parse_equation(equation):
    equation = equation.replace('>', '')
    equation = equation.replace('<', '')
    spt_equation = equation.split('=')
    reactant_str = spt_equation[0].strip()
    product_str = spt_equation[1].strip()

    reactant_info = {}
    product_info = {}
    metabolite_info = {}
    reactant_list = reactant_str.split(' + ')
    for each_metabolite in reactant_list:
        spt_metabolite = each_metabolite.strip().split(' ')
        if len(spt_metabolite) == 1:
            metabolite_name = spt_metabolite[0].strip()
            coff = 1.0
            reactant_info[metabolite_name] = coff * -1
        elif len(spt_metabolite) == 2:
            coff = spt_metabolite[0].strip()
            metabolite_name = spt_metabolite[1].strip()
            reactant_info[metabolite_name] = float(coff) * -1

    product_list = product_str.split(' + ')
    for each_metabolite in product_list:
        spt_metabolite = each_metabolite.strip().split(' ')
        if len(spt_metabolite) == 1:
            metabolite_name = spt_metabolite[0].strip()
            coff = 1.0
            product_info[metabolite_name] = float(coff)
        elif len(spt_metabolite) == 2:
            coff = spt_metabolite[0].strip()
            metabolite_name = spt_metabolite[1].strip()
            product_info[metabolite_name] = float(coff)

    return reactant_info, product_info


def generate_single_reaction(line):
    sptlist = line.strip().split('\t')
    entrez_gene_id = sptlist[0].strip()
    ensembl_transcript_id = sptlist[2].strip()
    ec_number = sptlist[6].strip()
    compartment = sptlist[8].strip()
    reaction_id = sptlist[10].strip()
    reaction_name = sptlist[11].strip()
    pathway = sptlist[12].strip()
    mnxref_equation = sptlist[14].strip()

    try:
        reactant_info, product_info = parse_equation(mnxref_equation)
    except:
        return False

    new_reaction_metabolite_obj = {}
    for each_reactant in reactant_info:
        metabolite_id = '%s_%s' % (each_reactant, compartment)
        metabolite_coefficient = reactant_info[each_reactant]
        new_metabolite_obj = Metabolite(metabolite_id, name=metabolite_id, compartment=compartment)
        new_reaction_metabolite_obj[new_metabolite_obj] = metabolite_coefficient

    for each_product in product_info:
        metabolite_id = '%s_%s' % (each_product, compartment)
        metabolite_coefficient = product_info[each_product]
        new_metabolite_obj = Metabolite(metabolite_id, name=metabolite_id, compartment=compartment)
        new_reaction_metabolite_obj[new_metabolite_obj] = metabolite_coefficient

    new_reaction_name = '%s_%s_%s_%s' % (reaction_id, ec_number, ensembl_transcript_id, compartment)
    reaction_obj = Reaction(new_reaction_name)
    reaction_obj.name = new_reaction_name
    reaction_obj.subsystem = pathway
    reaction_obj.lower_bound = -1000.0
    reaction_obj.upper_bound = 1000.0
    reaction_obj.objective_coefficient = 0
    reaction_obj.gene_reaction_rule = '(%s)' % (ensembl_transcript_id)
    reaction_obj.reversibility = 1
    reaction_obj.add_metabolites(new_reaction_metabolite_obj)
    return reaction_obj


def generate_cobra_model(getpra_file):
    logging.info("Generating GeTPRA model")
    start = time.time()
    
    reaction_count = 1
    reaction_list = []
    with open(getpra_file, 'r') as fp:
        fp.readline()
        cobra_model = Model('GeTPRA Model')
        for line in fp:
            reaction_obj = generate_single_reaction(line)
            sptlist = line.strip().split('\t')
            compartment = sptlist[8].strip()
            if reaction_obj != False:
                reaction_obj.id = 'GeTPRA%05d_%s' % (reaction_count, compartment)
                reaction_list.append(reaction_obj)
                reaction_count += 1

    cobra_model.add_reactions(reaction_list)
    
    logging.info( '%i reactions in GeTPRA model' % len(cobra_model.reactions))
    logging.info( '%i metabolites in GeTPRA model' % len(cobra_model.metabolites))
    logging.info( '%i genes in GeTPRA model' % len(cobra_model.genes))
    
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    return cobra_model


def generate_single_reaction_using_curated_information(reaction_information):
    reaction_id = reaction_information['id']
    reaction_name = reaction_information['name']
    genes = reaction_information['gene']
    compartment = reaction_information['compartment']
    subsystem = reaction_information['subsystem']
    reversibility = int(reaction_information['reversibility'])
    mnxref_equation = reaction_information['equation']

    if reversibility == -1:
        reaction_list = mnxref_equation.split('<=>')
        mnxref_equation = '%s <=> %s' % (reaction_list[1].strip(), reaction_list[0].strip())
        reversibility = 0

    try:
        reactant_info, product_info = parse_equation(mnxref_equation)
    except:
        return False

    new_reaction_metabolite_obj = {}
    for each_reactant in reactant_info:
        metabolite_id = '%s_%s' % (each_reactant, compartment)
        metabolite_coefficient = reactant_info[each_reactant]
        new_metabolite_obj = Metabolite(metabolite_id, name=metabolite_id, compartment=compartment)
        new_reaction_metabolite_obj[new_metabolite_obj] = metabolite_coefficient

    for each_product in product_info:
        metabolite_id = '%s_%s' % (each_product, compartment)
        metabolite_coefficient = product_info[each_product]
        new_metabolite_obj = Metabolite(metabolite_id, name=metabolite_id, compartment=compartment)
        new_reaction_metabolite_obj[new_metabolite_obj] = metabolite_coefficient

    new_reaction_name = reaction_name
    reaction_obj = Reaction(reaction_id)
    reaction_obj.name = new_reaction_name
    reaction_obj.subsystem = subsystem
    if reversibility == 1:
        reaction_obj.lower_bound = -1000.0
        reaction_obj.upper_bound = 1000.0
    else:
        reaction_obj.lower_bound = 0.0
        reaction_obj.upper_bound = 1000.0
    reaction_obj.objective_coefficient = 0
    reaction_obj.gene_reaction_rule = '(%s)' % (' or '.join(genes))
    reaction_obj.reversibility = reversibility
    reaction_obj.add_metabolites(new_reaction_metabolite_obj)

    return reaction_obj


def generate_cobra_model_for_final_addition(getpra_file):
    with open(getpra_file, 'r') as fp:
        fp.readline()
        reaction_information = {}
        for line in fp:
            sptlist = line.strip().split('\t')
            entrez_gene_id = sptlist[0].strip()
            ensembl_transcript_id = sptlist[2].strip()
            ec_number = sptlist[6].strip()
            compartment = sptlist[8].strip()
            reaction_id = sptlist[10].strip()
            reaction_name = sptlist[11].strip()
            pathway = sptlist[12].strip()
            mnxref_equation = sptlist[14].strip()
            reversibility = sptlist[18].strip()

            new_reaction_id = 'GeTPRA_%s_%s' % (reaction_id, compartment)
            if new_reaction_id not in reaction_information:
                reaction_information[new_reaction_id] = {}
                reaction_information[new_reaction_id]['id'] = new_reaction_id
                reaction_information[new_reaction_id]['name'] = reaction_name
                reaction_information[new_reaction_id]['gene'] = [entrez_gene_id]
                reaction_information[new_reaction_id]['compartment'] = compartment
                reaction_information[new_reaction_id]['subsystem'] = pathway
                reaction_information[new_reaction_id]['equation'] = mnxref_equation
                reaction_information[new_reaction_id]['reversibility'] = reversibility
            else:
                reaction_information[new_reaction_id]['gene'].append(entrez_gene_id)
                reaction_information[new_reaction_id]['gene'] = list(set(reaction_information[new_reaction_id]['gene']))

    reaction_list = []
    cobra_model = Model('GeTPRA Model')
    for each_reaction_information in reaction_information:
        reaction_obj = generate_single_reaction_using_curated_information(
            reaction_information[each_reaction_information])
        if reaction_obj != False:
            reaction_list.append(reaction_obj)
    if len(reaction_list) > 0:
        cobra_model.add_reactions(reaction_list)
        logging.info( '%i reactions in GeTPRA model' % len(cobra_model.reactions))
        logging.info( '%i metabolites in GeTPRA model' % len(cobra_model.metabolites))
        logging.info( '%i genes in GeTPRA model' % len(cobra_model.genes))
    return cobra_model
