import time
import logging

def arrange_reaction_equation(equation, rev):
    equation = equation.replace('>', '')
    equation = equation.replace('<', '')
    equation = equation.replace('=', '-')
    equation = equation.replace('--', '-')
    equation = equation.replace('==', '-')

    sptlist = equation.split('-')
    reactant_str = sptlist[0]
    product_str = sptlist[1]

    reactant_list = []
    product_list = []

    sptlist = reactant_str.split('+')
    for each_item in sptlist:
        spt_metabolite = each_item.strip().split(' ')
        if len(spt_metabolite) == 1:
            coff = float(1)
            mnx_met = spt_metabolite[0].strip().split('_')[0].strip()
        else:
            coff = float(1)
            mnx_met = spt_metabolite[1].strip().split('_')[0].strip()

        if mnx_met == 'MNXM1' or mnx_met == 'MNXM2':
            continue
        new_met = '%s %s' % (coff, mnx_met)
        reactant_list.append(new_met)

    sptlist = product_str.split('+')
    for each_item in sptlist:
        spt_metabolite = each_item.strip().split(' ')
        if len(spt_metabolite) == 1:
            coff = float(1)
            mnx_met = spt_metabolite[0].strip().split('_')[0].strip()
        else:
            coff = float(spt_metabolite[0].strip())
            mnx_met = spt_metabolite[1].strip().split('_')[0].strip()

        if mnx_met == 'MNXM1' or mnx_met == 'MNXM2':
            continue
        new_met = '%s %s' % (coff, mnx_met)
        product_list.append(new_met)

    reactant_list.sort()
    product_list.sort()

    rev_str = ''
    if rev == 1:
        rev_str = '<->'
    else:
        rev_str = '->'

    new_reaction = '%s %s %s' % (' + '.join(reactant_list), rev_str, ' + '.join(product_list))
    new_reaction2 = '%s %s %s' % (' + '.join(product_list), rev_str, ' + '.join(reactant_list))

    return new_reaction, new_reaction2


def compare_reaction(query_reaction_class, target_cobra_model):
    compartment_list = [metabolite.compartment for metabolite in
                        query_reaction_class.reactants + query_reaction_class.products]
    compartment_list = list(set(compartment_list))

    if len(compartment_list) > 1:
        return []

    query_compartment = compartment_list[0].strip()
    query_reaction = query_reaction_class.reaction
    reversibility = 1

    new_reaction, new_reaction2 = arrange_reaction_equation(query_reaction, 1)

    reaction_id_list = []
    for each_reaction in target_cobra_model.reactions:
        compartments = [metabolite.compartment for metabolite in each_reaction.reactants + each_reaction.products]
        compartments = list(set(compartments))

        if len(compartments) == 1:
            if len(each_reaction.reactants) >= 1 and len(each_reaction.products) >= 1:
                target_compartment_list = compartments
                target_compartment = target_compartment_list[0].strip()
                target_new_reaction, target_new_reaction2 = arrange_reaction_equation(each_reaction.reaction, 1)

                if (new_reaction == target_new_reaction) or (new_reaction2 == target_new_reaction):
                    if query_compartment == target_compartment:
                        reaction_id_list.append(each_reaction.id)

    return reaction_id_list


def check_redundancy(cobra_model, getpra_model):
    logging.info("Checking redundancy")    
    start = time.time()    
    redundant_reaction_information = {}
    cnt = 0
    total_reaction_cnt = len(cobra_model.reactions)
    for each_reaction in cobra_model.reactions:
        cnt += 1
        if cnt % (len(cobra_model.reactions)/10) == 0:            
            logging.info("Checking redundancy of GeTPRA reactions, %.2f %% (%s/%s) completed"%((cnt/float(len(cobra_model.reactions)))*100, cnt, len(cobra_model.reactions)))
        
        compartments = [metabolite.compartment for metabolite in each_reaction.reactants + each_reaction.products]
        compartments = list(set(compartments))
        if len(compartments) == 1 and 'e' not in compartments:        
            target_reaction_id_list = compare_reaction(each_reaction, getpra_model)
            if len(target_reaction_id_list) > 0:
                redundant_reaction_information[each_reaction.id] = target_reaction_id_list
    logging.info("Checking redundancy completed, "+time.strftime("elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    return redundant_reaction_information


def check_blocked_reaction(cobra_model, getpra_merged_model):
    logging.info("Checking functionality (blocked reaction)")
    start = time.time()    
    cobra_model_reactions = [each_reaction.id for each_reaction in cobra_model.reactions]
    
    all_reactions = []
    flux_constraints = {}
    for each_reaction in getpra_merged_model.reactions:
        if each_reaction.boundary == 'system_boundary':
            each_reaction.lower_bound = -1000.0
            each_reaction.upper_bound = 1000.0
        
        if each_reaction.id not in cobra_model_reactions:
            each_reaction.lower_bound = 0.0
            each_reaction.upper_bound = 0.0
            
            flux_constraints[each_reaction.id] = [0.0, 0.0]
     
    reaction_functionality_info = {}    
    total_reaction_cnt = len(flux_constraints)
    
    all_reactions = flux_constraints.keys()
    cnt = 1    
    for each_reaction in all_reactions:
        if cnt % (len(all_reactions)/10) == 0:            
            logging.info("Checking functionality of GeTPRA reactions, %.2f %% (%s/%s) completed"%((cnt/float(len(all_reactions)))*100, cnt, len(all_reactions)))
        
        getpra_merged_model.objective = each_reaction
        
        idx = getpra_merged_model.reactions.index(each_reaction)
        getpra_merged_model.reactions[idx].lower_bound = -1000.0
        getpra_merged_model.reactions[idx].upper_bound = 1000.0
        
        getpra_merged_model.optimize(objective_sense='maximize')
        max_obj_value = getpra_merged_model.solution.f
        
        getpra_merged_model.optimize(objective_sense='minimize')
        min_obj_value = getpra_merged_model.solution.f        

        getpra_merged_model.reactions[idx].lower_bound = 0.0
        getpra_merged_model.reactions[idx].upper_bound = 0.0
        
        reaction_functionality_info[each_reaction] = [min_obj_value, max_obj_value]            
        cnt += 1
    logging.info("Checking functionality (blocked reaction) completed, "+time.strftime("elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    return reaction_functionality_info
