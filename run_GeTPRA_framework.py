import argparse
import os
import logging
import time

from cobra.io import read_sbml_model
from cobra.io import write_sbml_model
from getpra import io
from getpra.evidence_database import BRENDA
from getpra.evidence_database import HPA
from getpra.evidence_database import UniProt
from getpra.preprocessing import EFICAz
from getpra.preprocessing import WoLFPSort
from getpra.reaction_database import KEGG
from getpra.framework import getpra_management
from getpra.framework import cobra_model_generation
from getpra.framework import functionality

def prepare_GeTPRA_data(output_dir, EFICAz25_output_file, WoLFPSort_output_file, configuration_options):
    logging.info("*Get reactions from biochemical database using EC number")
    
    start = time.time()
    BRENDA_user_email = configuration_options['BRENDA_user_email']
    BRENDA_user_password = configuration_options['BRENDA_user_password']

    WoLFPSort_result_file = WoLFPSort_output_file
    Ensembl_mapping_file = configuration_options['Ensembl_mapping_file']
    WoLFPSort_output_file = output_dir + 'PREDICTION_WoLFPSort_localization_result.txt'

    EFICAz_result_file = EFICAz25_output_file
    EFICAz_output_file = output_dir + 'PREDICTION_EFICAz_EC_prediction_result.txt'
    ec_number_file = output_dir + 'ec_numbers.txt'

    WoLFPSort.parse_WoLFPSort_result(WoLFPSort_result_file, Ensembl_mapping_file, WoLFPSort_output_file)
    EFICAz.parse_EFICAz_result(EFICAz_result_file, EFICAz_output_file, ec_number_file)

    mnxref_com_xref_file = configuration_options['mnx_chem_xref_file']
    kegg_reaction_output_file = output_dir + 'REACTION_KEGG_human.txt'
    KEGG.download_KEGG_data(ec_number_file, mnxref_com_xref_file, kegg_reaction_output_file)    
    logging.info("*Standardize metabolite information")
    logging.info("*Compartmentalize metabolic reactions")
    
    HPA_output_file = configuration_options['HPA']    
    HPA_compartment_output_file = output_dir + 'EVIDENCE_SL_HPA_subcellular_location.txt'

    HPA.parse_HPA_file(HPA_output_file, HPA_compartment_output_file)

    brenda_output_file = output_dir + 'EVIDENCE_EC_BRENDA_UniProtIDs.txt'
    BRENDA.download_BRENDA_data(ec_number_file, brenda_output_file, BRENDA_user_email, BRENDA_user_password)

    UniProt_output_file = output_dir + 'EVIDENCE_EC_UniProt_Reviewd_Human_Proteins.txt'
    UniProt.download_UniProt_data(UniProt_output_file)
    
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))

def generate_GeTPRA(output_dir, configuration_options):
    logging.info("*Generate GeTPRA")
    start = time.time()
    Ensembl_mapping_file = configuration_options['Ensembl_mapping_file']
    NCBI_mapping_file = configuration_options['NCBI_mapping_file']
    APPRIS_file = configuration_options['APPRIS_file']

    metabolic_model = configuration_options['metabolic_model']
    cobra_model = read_sbml_model(metabolic_model)
    cobra_model_genes = [gene.id for gene in cobra_model.genes]

    # Predictions
    EFICAz_result = output_dir + 'PREDICTION_EFICAz_EC_prediction_result.txt'
    WoLFPSort_result = output_dir + 'PREDICTION_WoLFPSort_localization_result.txt'

    # Evidences
    KEGG_result = output_dir + 'REACTION_KEGG_human.txt'
    BRENDA_result = output_dir + 'EVIDENCE_EC_BRENDA_UniProtIDs.txt'
    UniProt_result = output_dir + 'EVIDENCE_EC_UniProt_Reviewd_Human_Proteins.txt'
    HPA_result = output_dir + 'EVIDENCE_SL_HPA_subcellular_location.txt'

    
    ensembl_id_mapping_information = io.read_gene_id_mapping_file(Ensembl_mapping_file, NCBI_mapping_file)    
    principal_transcript_information = io.read_APPRIS_file(APPRIS_file)    
    ec_prediction_info = io.read_EC_prediction_result(EFICAz_result)    
    sl_prediction_info = io.read_SL_prediction_result(WoLFPSort_result)    
    sl_evidence_info = io.read_SL_evidence_result(HPA_result)    
    ec_evidence_info, transcript_uniprot_info = io.read_EC_evidence_result(BRENDA_result, UniProt_result)    
    reaction_info = io.read_reaction_information_file(KEGG_result)
    
    check_redundancy_information = {}
    fp = open(output_dir + 'draft_GeTPRA.txt', 'w')
    print >> fp, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' % (
        'Entrez gene ID', 'Ensembl gene ID',
        'Ensembl transcript ID', 'RefSeq ID', 'UCSC ID',
        'Transcript type', 'Predicted EC number',
        'Experimental evidences on ECs (UnitProt ID)', 'Predicted SL', 'Experimental evidences on SLs',
        'KEGG reaction ID',
        'KEGG reaction name', 'KEGG pathway', 'KEGG equation', 'MNXref equation', 'Definition')
    
    for ensembl_transcript_id in ec_prediction_info:
        predicted_ec_list = ec_prediction_info[ensembl_transcript_id]
        for predicted_ec in predicted_ec_list:            
            if ensembl_transcript_id in ensembl_id_mapping_information:
                if ensembl_transcript_id in sl_prediction_info:
                    ec_evidence = 'N/A'
                    evidence_uniprot_list = []

                    if predicted_ec in ec_evidence_info:
                        ensembl_transcripts_with_evidence = ec_evidence_info[predicted_ec]
                        for each_transcript in ensembl_transcripts_with_evidence:
                            if each_transcript == ensembl_transcript_id:
                                eveidence_uniprot_ids = transcript_uniprot_info[each_transcript]
                                evidence_uniprot_list.append(';'.join(eveidence_uniprot_ids))

                    evidence_uniprot_list = list(set(evidence_uniprot_list))
                    if len(evidence_uniprot_list) > 0:
                        ec_evidence = ';'.join(evidence_uniprot_list)

                    predicted_sl_list = sl_prediction_info[ensembl_transcript_id]
                    kegg_reaction_list = []

                    if predicted_ec in reaction_info:
                        kegg_reactions = reaction_info[predicted_ec]
                    if len(kegg_reactions) > 0:
                        kegg_reaction_list = kegg_reactions
                    else:
                        record = {}
                        record['ec_number'] = 'N/A'
                        record['reaction_id'] = 'N/A'
                        record['reaction_name'] = 'N/A'
                        record['pathway'] = 'N/A'
                        record['kegg_equation'] = 'N/A'
                        record['mnxref_equation'] = 'N/A'
                        record['definition'] = 'N/A'
                        kegg_reaction_list = [record]

                    entrez_gene_id_list = ensembl_id_mapping_information[ensembl_transcript_id]['entrez_gene_id']
                    ensembl_gene_id = ensembl_id_mapping_information[ensembl_transcript_id]['ensembl_gene_id']
                    refseq_id = ensembl_id_mapping_information[ensembl_transcript_id]['refseq_id']
                    ucsc_id = ensembl_id_mapping_information[ensembl_transcript_id]['ucsc_id']

                    for entrez_gene_id in entrez_gene_id_list:
#                         if entrez_gene_id not in cobra_model_genes:
#                             continue
                        is_principal_transcript = ensembl_transcript_id in principal_transcript_information
                        if is_principal_transcript:
                            is_principal_transcript = 'PT (%s)' % (
                                principal_transcript_information[ensembl_transcript_id])
                        else:
                            is_principal_transcript = 'NPT'

                        for each_sl in predicted_sl_list:
                            sl_evidence = 'N/A'
                            if ensembl_gene_id in sl_evidence_info:
                                sl_evidence_list = sl_evidence_info[ensembl_gene_id]
                                if each_sl in sl_evidence_list:
                                    sl_evidence = 'Yes'
                                else:
                                    sl_evidence = 'N/A'

                            for each_kegg_data in kegg_reaction_list:
                                reaction_id = each_kegg_data['reaction_id']
                                reaction_name = each_kegg_data['reaction_name']
                                pathway = each_kegg_data['pathway']
                                kegg_equation = each_kegg_data['kegg_equation']
                                mnxref_equation = each_kegg_data['mnxref_equation']
                                definition = each_kegg_data['definition']

                                redundancy_key = '%s_%s_%s_%s' % (
                                    ensembl_gene_id, ensembl_transcript_id, each_sl, reaction_id)
                                if redundancy_key not in check_redundancy_information:
                                    check_redundancy_information[redundancy_key] = redundancy_key
                                    print >> fp, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' % (
                                        entrez_gene_id, ensembl_gene_id,
                                        ensembl_transcript_id, refseq_id, ucsc_id,
                                        is_principal_transcript, predicted_ec,
                                        ec_evidence, each_sl, sl_evidence,
                                        reaction_id, reaction_name, pathway,
                                        kegg_equation, mnxref_equation, definition)

    fp.close()
    logging.info("Generating GeTPRA input data completed, "+time.strftime("elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    return

def check_functionality_and_evidence(output_dir, cobra_model_file):    
    draft_getpra_file = output_dir + 'draft_GeTPRA.txt'    
    getpra_cobra_model = cobra_model_generation.generate_cobra_model(draft_getpra_file)
    write_sbml_model(getpra_cobra_model, output_dir + 'getpra_cobra_model.xml', use_fbc_package=False)
    
    cobra_model = read_sbml_model(cobra_model_file)
    getpra_cobra_model = read_sbml_model(output_dir + 'getpra_cobra_model.xml')
        
    redundant_reaction_information = functionality.check_redundancy(cobra_model, getpra_cobra_model)
    with open(output_dir + 'redundant_reactions.txt', 'w') as fp:
        print >> fp, 'Recon reaction ID\tGeTPRA reaction ID'
        for each_cobra_reaction in redundant_reaction_information:
            for each_redundant_reaction in redundant_reaction_information[each_cobra_reaction]:
                print >> fp, '%s\t%s' % (each_cobra_reaction, each_redundant_reaction)
    
    cobra_model.add_reactions(getpra_cobra_model.reactions)
    write_sbml_model(cobra_model, output_dir + 'getpra_merged_model.xml', use_fbc_package=False)

    cobra_model = read_sbml_model(cobra_model_file)
    getpra_merged_cobra_model = read_sbml_model(output_dir + 'getpra_merged_model.xml')
    
    reaction_functionality_info = functionality.check_blocked_reaction(cobra_model, getpra_merged_cobra_model)    
    
    with open(output_dir + 'reaction_functionality.txt', 'w') as fp:
        print >> fp, 'GeTPRA reaction ID\tFunctionality\tMin\tMax'
        for each_reaction in reaction_functionality_info:
            reaction_functionality = False
            if abs(reaction_functionality_info[each_reaction][0]) > 0 or abs(
                    reaction_functionality_info[each_reaction][1]) > 0:
                reaction_functionality = True
            print >> fp, '%s\t%s\t%s\t%s' % (
                each_reaction, reaction_functionality, reaction_functionality_info[each_reaction][0],
                reaction_functionality_info[each_reaction][1])
    
    
    logging.info("Summarizing results")
    reaction_functionality_file = output_dir + 'reaction_functionality.txt'
    redundant_reaction_file = output_dir + 'redundant_reactions.txt'
    updated_getpra_file = output_dir + 'GeTPRA.txt'
    getpra_management.update_functionality_information_to_GeTPRA(getpra_cobra_model, draft_getpra_file, reaction_functionality_file,
                                    redundant_reaction_file, updated_getpra_file)    

    cobra_model = read_sbml_model(cobra_model_file)        
    getpra_management.check_evidence_and_functionality(output_dir, updated_getpra_file)    
    getpra_management.check_GPR_evidence(output_dir, cobra_model, updated_getpra_file)
    

def main():
    start = time.time()
    parser = argparse.ArgumentParser()

    parser.add_argument('-output_dir', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-ec', '--EFICAz25_output_file', required=True, help="EFICAz output file")
    parser.add_argument('-sl', '--WoLFPSort_output_file', required=True, help="WoLFPSort output file")

    parser.add_argument('-brenda_email', '--BRENDA_user_email', required=True, help="BRENDA user email address")
    parser.add_argument('-brenda_pw', '--BRENDA_user_pw', required=True, help="BRENDA user password")
    parser.add_argument('-ensembl', '--Ensembl_mapping_file', required=True, help="Ensembl mapping file")
    parser.add_argument('-appris', '--APPRIS_file', required=True, help="APPRIS information file")
    parser.add_argument('-model', '--cobra_model_file', required=True, help="Cobra model file")
    parser.add_argument('-mnx_xref', '--MNXref_metabolite_xref', required=True, help="MNXref xref file")
    parser.add_argument('-hpa', '--HPA_sl_file', required=True, help="SL information in HPA")
    parser.add_argument('-ncbi_id_information', '--NCBI_id_mapping_file', required=True,
                        help="Gene ID mapping information file from NCBI ftp")

    
    options = parser.parse_args()    
    
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    
    output_dir = options.output_dir
    WoLFPSort_output_file = options.WoLFPSort_output_file
    EFICAz25_output_file = options.EFICAz25_output_file

    configuration_options = {}
    configuration_options['BRENDA_user_email'] = options.BRENDA_user_email
    configuration_options['BRENDA_user_password'] = options.BRENDA_user_pw
    configuration_options['Ensembl_mapping_file'] = options.Ensembl_mapping_file
    configuration_options['mnx_chem_xref_file'] = options.MNXref_metabolite_xref
    configuration_options['Ensembl_mapping_file'] = options.Ensembl_mapping_file
    configuration_options['APPRIS_file'] = options.APPRIS_file
    configuration_options['metabolic_model'] = options.cobra_model_file
    configuration_options['HPA'] = options.HPA_sl_file
    configuration_options['NCBI_mapping_file'] = options.NCBI_id_mapping_file    
    
    BRENDA_connection_status = BRENDA.check_BRENDA_connection(options.BRENDA_user_email, options.BRENDA_user_pw)
    
    if BRENDA_connection_status:
        cobra_model_file = options.cobra_model_file
        cobra_model = read_sbml_model(cobra_model_file)

        try:
            os.mkdir(output_dir)
        except:
            pass

        prepare_GeTPRA_data(output_dir, EFICAz25_output_file, WoLFPSort_output_file, configuration_options)
        generate_GeTPRA(output_dir, configuration_options)    
        check_functionality_and_evidence(output_dir, cobra_model_file)        
    
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    
if __name__ == '__main__':
    main()
